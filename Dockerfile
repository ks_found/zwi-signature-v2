FROM debian:11-slim

RUN apt-get update && apt-get install -y \
   python3 python3-cryptography python3-jwt python3-requests python-is-python3 \
   openssl zip unzip \
   && rm -rf /var/lib/apt/lists/*

RUN mkdir -p /zwi /app

COPY . /app/

RUN if [ ! -f "/app/es384.pem" ]; then openssl ecparam -name secp384r1 -genkey -noout -out /app/es384.pem; fi

WORKDIR /zwi

RUN echo "alias zwisign='python3 /app/zwi_signature.py -k /app/es384.pem -c /app/signature.conf \"\$@\"'" >> ~/.bashrc

ENTRYPOINT ["python3", "/app/zwi_signature.py", "-k", "/app/es384.pem", "-c", "/app/signature.conf"]